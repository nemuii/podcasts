## Podcasts release proccess

* Ensure there was a post-release version bump last time
* Update CHANGELOG.md
* Edit appdata.xml with the correct version and release notes
* commit and tag in git
* make a tarball for flathub
* Post-release version bump meson.build

